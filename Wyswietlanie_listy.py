import shelve,datetime


def show_list():
    today = str(datetime.date.today())
    db = shelve.open('baza')
    print("Czy chcesz wyświetlić dzisiejszą listę obecności? y/n")
    choice = input()
    if choice == "y":
        print(today)
        for i in db[today]:
            print(i[0], " " * (40 - len(i[0])), i[1])
    else:
        print("Wpisz datę z wyświetlonych poniżej:")
        for k in db:
            print(k)
        date = input()
        print(date)
        for i in db[date]:
            print(i[0], " " * (40 - len(i[0])), i[1])