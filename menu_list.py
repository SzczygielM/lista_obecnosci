import check_present
import Wyswietlanie_listy
import add_person
list_menu = ["0. Wyjście", "1. Sprawdź obecność", "2. Dodaj ucznia do listy", "3. Przeglądaj listy obecności"]

class Menu:
    def __init__(self):
        self.display_Menu()

    def display_Menu(self):
        print("Witam w programie lista obecności!\nOto główne menu")
        choice = ""
        while choice!= 0:
            for i in list_menu:
                print(i)
            choice = int(input("Podaj numer co chcesz zrobić"))
            if choice == 1:
                check_present.check_present()
            elif choice == 2:
                add_person.add_person()
            elif choice ==3:
                Wyswietlanie_listy.show_list()
            elif choice == 0:
                print("EXIT")
            else:
                print("Wpisałeś niepoprawny numer!")

